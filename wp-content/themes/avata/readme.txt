Avata theme
============


Copyrights for Resources used in this theme.
============

The theme itself is nothing but 100% GPL v2 or later.

== jQuery Plugins

 ## bootstrap
   * Copyright 2011-2015 Twitter, Inc.
   * Licensed under the MIT license
     http://getbootstrap.com

 ## hoverIntent
   * Copyright 2007, 2013 Brian Cherne
   * Licensed under the MIT license
     http://cherne.net/brian/resources/jquery.hoverIntent.html

 ## jQuery Superfish Menu Plugin
   * Copyright (c) 2013 Joel Birch
   * Dual licensed under the MIT and GPL licenses
   
 ## jquery.meanmenu.js
  * Copyright (C) 2012-2014 Chris Wharton @ MeanThemes (https://github.com/meanthemes/meanMenu)
  * GNU General Public License

== Fonts

  ##  font-awesome
    Copyright: Font Awesome 4.4.0 by @davegandy
    License -  Font: SIL OFL 1.1, CSS: MIT License
    http://fontawesome.io/license

  ## Genericons
     Copyright (C) 1989, 1991 Free Software Foundation, Inc.,
    License -  GNU GENERAL PUBLIC LICENSE Version 2
    http://genericons.com/
    
 ==PHP

## Customizer Library
   @author         Devin Price, The Theme Foundry
   @license        GPL-2.0+
    https://github.com/devinsays/customizer-library