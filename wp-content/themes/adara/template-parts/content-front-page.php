<article id="post-<?php the_ID(); ?>" class="front-page-content">
    <img class="middle-image" src="<?php echo get_template_directory_uri(); ?>/img/bg2.gif">
    <div class="entry-content container ">
        <?php
        the_content();
        ?>
    </div><!-- .entry-content -->

</article><!-- #post-## -->
