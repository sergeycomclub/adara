<div class="article-top-margin"></div>
<article id="post-<?php the_ID(); ?>" class="single-post-news">
    <div class="container">
        <div class="date-single-post"><? the_date() ?></div>
        <hr>
        <div class="row">
            <div class="col s12 m12 l5 single-post-thumbnail"><?php twentysixteen_post_thumbnail(); ?></div>
            <div class="col s12 m12 l7">
                    <?php the_title('<h1 class="single-post-title">', '</h1>'); ?>

                <div class="entry-content">
                    <div class="clearfix">
                        <?php the_content();?>
                    </div>
                </div><!-- .entry-content -->
            </div>
        </div>
        <hr class="single-post-hr-bottom">
    </div>
</article><!-- #post-## -->
