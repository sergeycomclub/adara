<div class="row">
    <div class="block-icons col s12 m6 l6">
        <div class="block-icons-1">
            <div class="block-icons-title">
                <h3>Qualitative approachs</h3>
            </div>
            <div class="icons-container">
                <div class="icon-wrapper">
                    <div class="icon-image"><img src="/wp-content/themes/adara/img/ONLINE-DISCUSSIONS.svg"
                                                 alt="">
                    </div>
                    <div class="icon-title">ONLINE DISCUSSIONS</div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon-image"><img src="/wp-content/themes/adara/img/MYSTERY-SHOPPING.svg" alt="">
                    </div>
                    <div class="icon-title">MYSTERY SHOPPING</div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon-image"><img src="/wp-content/themes/adara/img/ONE-ON-ONE.svg" alt=""></div>
                    <div class="icon-title">ONE-ON-ONE</div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon-image"><img src="/wp-content/themes/adara/img/FOCUS-GROUPS.svg" alt="">
                    </div>
                    <div class="icon-title">
                        <div>FOCUS</div>
                        <div>GROUP</div>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon-image"><img
                            src="/wp-content/themes/adara/img/ETHNOGRAPHY VIDEOGRAPHY OBSERVATIONAL RESEARCH.svg"
                            alt="">
                    </div>
                    <div class="icon-title">
                        <div>ETHNOGRAPHY</div>
                        <div>VIDEOGRAPHY</div>
                        <div>OBSERVATIONAL</div>
                        <div>RESEARCH</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block-icons col s12 m6 l6">
        <div class="block-icons-2">
            <div class="block-icons-title"><h3>QUANTITATIVE APPROACHES</h3></div>
            <div class="icons-container">
                <div class="icon-wrapper">
                    <div class="icon-image"><img src="/wp-content/themes/adara/img/Online Survey .svg" alt="">
                    </div>
                    <div class="icon-title">
                        <div>ONLINE</div>
                        <div>SURVEYS</div>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon-image"><img src="/wp-content/themes/adara/img/PREDICTIVE MODELING.svg"
                                                 alt="">
                    </div>
                    <div class="icon-title">
                        <div>PREDICTIVE</div>
                        <div>MODELING</div>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon-image"><img src="/wp-content/themes/adara/img/TELEPHONE.svg" alt=""></div>
                    <div class="icon-title">TELEPHONE</div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon-image"><img src="/wp-content/themes/adara/img/MAIL SURVEYS.svg" alt="">
                    </div>
                    <div class="icon-title">MAIL SURVEYS</div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon-image"><img src="/wp-content/themes/adara/img/TEXT ANALYTICS.svg" alt="">
                    </div>
                    <div class="icon-title">
                        <div>TEXT</div>
                        <div>ANALYTICS</div>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon-image"><img src="/wp-content/themes/adara/img/DATA MINING.svg" alt="">
                    </div>
                    <div class="icon-title">
                        <div>DATA</div>
                        <div>MINING</div>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon-image"><img src="/wp-content/themes/adara/img/MOBILE AND SMS.svg" alt="">
                    </div>
                    <div class="icon-title">
                        <div>MOBILE</div>
                        <div>AND SMS</div>
                    </div>
                </div>
                <div class="icon-wrapper">
                    <div class="icon-image"><img src="/wp-content/themes/adara/img/SOCIAL MEDIA MONITORING.svg"
                                                 alt="">
                    </div>
                    <div class="icon-title">
                        <div>SOCIAL MEDIA</div>
                        <div>MONITORING</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>