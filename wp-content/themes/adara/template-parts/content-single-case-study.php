<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<article id="post-<?php the_ID(); ?>" class="case-study-top-margin">
    <div class="post-slider container">
        <div class="post-title">
            <?= get_the_title() ?>
            <?
            $args = array(
                'post_type' => 'case_studies',
                'order' => 'ASC'
            );

            $pagelist = get_posts($args);

            $pages = array();

            foreach ($pagelist as $page) {
                $pages[] += $page->ID;
            }

            $current = array_search(get_the_ID(), $pages);
            $prevID = $pages[$current - 1];
            if ($prevID == null) $prevID = end($pages);
            $nextID = $pages[$current + 1];
            if ($nextID == null) $nextID = $pages[0];
            ?>
            <a class="left arrow" href="<?= get_permalink($prevID); ?>"><i
                    class="material-icons">keyboard_arrow_left</i></a>
            <a class="right arrow" href="<?= get_permalink($nextID); ?>"><i
                    class="material-icons">keyboard_arrow_right</i></a>
        </div>
        <hr>
        <div class="type-wrapper">
            <?= get_field('type', $case_study->ID) ?>
            <img src="<?= get_field('type_icon', $case_study->ID)['url'] ?>" alt="">
        </div>
        <div class="reserch-objective-title">
            <?= get_field('description', $case_study->ID) ?>
        </div>
        <div class="research_objectives_title">RESEARCH OBJECTIVES:</div>
        <div class="research-objective-content row">
            <div class="col s12 m12 l7">
                <?= get_field('reserch_objectives', $case_study->ID) ?>
            </div>
            <div class="col s12 m12 l5 research-objectives-right-part">
                <? for ($i = 1; $i <= 4; $i++): ?>
                    <div class="clearfix">
                        <div class="left-image-text flex-row-vcenter ">
                            <? if (!empty(get_field('research_objectives_right_part_' . $i . '_image', $case_study->ID)['url'])): ?>
                                <img
                                    src="<?= get_field('research_objectives_right_part_' . $i . '_image', $case_study->ID)['url'] ?>"
                                    alt="">
                            <? endif ?>
                            <? if (get_field('research_objectives_right_part_' . $i . '_text', $case_study->ID)): ?>
                                <div class="text-wrapper">
                                    <?= get_field('research_objectives_right_part_' . $i . '_text', $case_study->ID) ?>
                                </div>
                            <? endif ?>
                        </div>
                    </div>
                <? endfor ?>
            </div>
        </div>
    </div>
    <div class="white-full-width">
        <div class="container">
            <div class="white-row row">
                <? for ($i = 1; $i <= 4; $i++): ?>
                    <? if (!empty(get_field('white_area_' . $i . '_column_image', $case_study->ID)['url']) ||
                        !empty(get_field('white_area_' . $i . '_column_title', $case_study->ID)) ||
                        !empty(get_field('white_area_' . $i . '_column_content', $case_study->ID))
                    ): ?>
                        <div class="col s12 m6 l4">
                            <div class="white-element">
                                <img
                                    src="<?= get_field('white_area_' . $i . '_column_image', $case_study->ID)['url'] ?>"
                                    alt="">
                                <div class="white-element-title">
                                    <?= get_field('white_area_' . $i . '_column_title') ?>
                                </div>
                                <div class="white-element-description">
                                    <?= get_field('white_area_' . $i . '_column_description') ?>
                                </div>
                                <div class="white-element-content">
                                    <?= get_field('white_area_' . $i . '_column_content') ?>
                                </div>
                            </div>
                        </div>
                    <? endif ?>
                <? endfor ?>
            </div>
        </div>
    </div>
</article><!-- #post-## -->
