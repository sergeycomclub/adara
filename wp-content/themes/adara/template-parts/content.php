<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="container">
        <div class="row">
            <a href="<?= get_permalink() ?>">
                <div class="col s12 m5 l5 post-thumbnail"><?php twentysixteen_post_thumbnail(); ?></div>
            </a>
            <div class="col s12 m7 l7 post-excerpt">
                <div class="date"><? the_date() ?></div>
                <hr>
                <header class="entry-header">
                    <?php the_title(sprintf('<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>'); ?>
                </header>
                <div class="entry-content">
                    <?php
                    echo the_excerpt();

                    ?>
                </div>
            </div>
        </div>
    </div>
</article>