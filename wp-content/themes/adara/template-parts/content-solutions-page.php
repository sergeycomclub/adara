<article id="post-<?php the_ID(); ?>" class="article-top-margin solutions-page-content">
    <!--    <img class="middle-image" src="--><?php //echo get_template_directory_uri(); ?><!--/img/bg2.gif">-->
    <div class="entry-content container ">
        <?php
        the_content();

        wp_link_pages(array(
            'before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'twentysixteen') . '</span>',
            'after' => '</div>',
            'link_before' => '<span>',
            'link_after' => '</span>',
            'pagelink' => '<span class="screen-reader-text">' . __('Page', 'twentysixteen') . ' </span>%',
            'separator' => '<span class="screen-reader-text">, </span>',
        ));
        ?>
    </div><!-- .entry-content -->
    <?
    $page = get_page_by_path('what-can-we-do-for-you');
    $page_id = $page->ID;
    $content_post = get_post($page_id);
    $content = $content_post->post_content;
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    echo $content;
    ?>
</article><!-- #post-## -->