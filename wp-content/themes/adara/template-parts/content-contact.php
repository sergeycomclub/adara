<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" class="article-top-margin">

    <?php twentysixteen_post_thumbnail(); ?>

    <div class="entry-content container">
        <?php
        the_content();
        ?>
    </div><!-- .entry-content -->
</article><!-- #post-## -->
