<article id="post-<?php the_ID(); ?>" class="article-top-margin optimization-page-content">
    <!--    <img class="middle-image" src="--><?php //echo get_template_directory_uri(); ?><!--/img/bg2.gif">-->
    <div class="entry-content container ">
        <?php
        the_content();

        wp_link_pages(array(
            'before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'twentysixteen') . '</span>',
            'after' => '</div>',
            'link_before' => '<span>',
            'link_after' => '</span>',
            'pagelink' => '<span class="screen-reader-text">' . __('Page', 'twentysixteen') . ' </span>%',
            'separator' => '<span class="screen-reader-text">, </span>',
        ));
        ?>
    </div><!-- .entry-content -->

    <?
    $optimization_page = get_page_by_path('optimization-blackicons');
    $optimization_page_id = $optimization_page->ID;
    $content_post = get_post($optimization_page_id);
    $optimization_content = $content_post->post_content;
    $optimization_content = apply_filters('the_content', $optimization_content);
    $optimization_content = str_replace(']]>', ']]&gt;', $optimization_content);
    ?>
    <? echo $optimization_content; ?>
    <div class="full-width-grey clearfix">
        <div class="container row">
            <div class="flex-row-flow post-blocks">
                <?

                $args = array(
                    'numberposts' => 10,
                    'post_type' => 'case_studies',
                    'order' => 'ASC'
                );

                $case_studies = get_posts($args);

                ?>
                <? foreach ($case_studies as $case_study): ?>
                    <div class="col s12 m6 l6">
                        <div class="post-wrapper">
                            <a href="<?= $case_study->guid ?>">
                                <img src="<?= get_the_post_thumbnail_url($case_study->ID) ?>" alt="">
                                <div class="post-popup-button">
                                    <div class="post-block-title"><?= $case_study->post_title ?></div>
                                    <div
                                        class="post-block-description"><?=
                                        get_field('description', $case_study->ID)
                                        ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                <? endforeach ?>
            </div>
        </div>
    </div>
</article><!-- #post-## -->
