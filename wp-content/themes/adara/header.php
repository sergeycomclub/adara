<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title><?php bloginfo('name'); ?></title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?= get_template_directory_uri() . '/css/materialize.css' ?>" type="text/css" rel="stylesheet"
          media="screen,projection"/>
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" type="text/css" rel="stylesheet"
          media="screen,projection"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,600" rel="stylesheet">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php if (is_singular() && pings_open(get_queried_object())) : ?>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php endif; ?>
    <?php wp_head(); ?>
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" type="text/css" rel="stylesheet"
          media="screen,projection"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

</head>
<body <?php body_class(); ?>>
<nav class="main-top-navigation" role="navigation">
    <div class="nav-wrapper container">

        <a href="http://www.adara.calceron.com/" class="custom-logo-link" rel="home" itemprop="url">
            <img class="" src="<?= get_template_directory_uri() ?>/img/adara-logo.svg" alt="Adara Logo">
        </a>
        <?php
        wp_nav_menu(array(
            'theme_location' => 'primary',
            'menu_class' => 'right hide-on-med-and-down primary-menu animated-menu'
        ));
        ?>

        <?php
        wp_nav_menu(array(
            'theme_location' => 'primary',
            'menu_class' => 'side-nav',
            'menu_id' => 'nav-mobile'
        ));
        ?>

        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
</nav>
<div class="scroll-block">
    <div class="main-container">
