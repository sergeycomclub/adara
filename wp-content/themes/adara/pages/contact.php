<?
/*
Template Name: Contact
*/
get_header(); ?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php
        // Start the loop.
        while (have_posts()) : the_post();

            // Include the page content template.
            get_template_part('template-parts/content', 'contact');

        endwhile;
        ?>
    </main><!-- .site-main -->
        <div class="image-with-bg contact-page-bg">
            <div class="overlay"></div>
            <img src="/wp-content/themes/adara/img/contact-bg.png" alt="">
        </div>

</div><!-- .content-area -->
<?php get_footer(); ?>
