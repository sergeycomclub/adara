<?
/*
Template Name: Solutions
*/
get_header(); ?>
<script
    src="https://code.jquery.com/jquery-1.12.4.min.js"
    integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
    crossorigin="anonymous"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/constellation.min.js"></script>
    <div class="constellation-wrapper constellation-grey-wrapper">
        <canvas class="constellation-grey"></canvas>
    </div>
<script src="<?php echo get_template_directory_uri(); ?>/js/stars-grey-init.js"></script>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php
        // Start the loop.
        while (have_posts()) : the_post();

            // Include the page content template.
            get_template_part('template-parts/content', 'solutions-page');

        endwhile;
        ?>

    </main><!-- .site-main -->
</div><!-- .content-area -->
<?php get_footer(); ?>
