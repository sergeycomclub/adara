<?
/*
Template Name: Mission
*/
get_header(); ?>
<style>
    nav.main-top-navigation {
        background-color: rgba(127, 119, 115, 0);
    }
    nav.main-top-navigation.small-top-menu {
        background-color: rgba(127, 119, 115, 0.98);
    }
</style>
<div class="constellation-wrapper mission-wrapper">
    <canvas class="constellation mission"></canvas>
</div>
<script src="<?php echo get_template_directory_uri(); ?>/js/round-animation.js"></script>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php
        // Start the loop.
        while (have_posts()) : the_post();

            // Include the page content template.
            get_template_part('template-parts/content', 'mission-page');

        endwhile;
        ?>


    </main><!-- .site-main -->

</div><!-- .content-area -->
<?php get_footer(); ?>
