<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php
        // Start the loop.
        while (have_posts()) : the_post();

            // Include the single post content template.
            get_template_part('template-parts/content', 'single');
            ?>
            <div class="container">
                <div class="row hide">
                    <div class="col s12 m6 l6">
                        <?php
                        // If comments are open or we have at least one comment, load up the comment template.
                        if (comments_open() || get_comments_number()) {
                            comments_template();
                        }
                        ?>
                    </div>
                </div>
                <?php
                if (is_singular('attachment')) {
                    // Parent post navigation.
                    the_post_navigation(array(
                        'prev_text' => _x('<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'twentysixteen'),
                    ));
                } elseif (is_singular('post')) {
                    // Previous/next post navigation.
                    the_post_navigation(array(
                        'next_text' => '<span class="meta-nav" aria-hidden="true">' . __('Next', 'twentysixteen') ."<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>".'</span>',
                        'prev_text' => '<span class="meta-nav" aria-hidden="true">' ."<i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>". __('Previous', 'twentysixteen') . '</span> '
                    ));
                }
                ?>
            </div>
            <?php
            // End of the loop.
        endwhile;
        ?>

    </main><!-- .site-main -->

    <?php get_sidebar('content-bottom'); ?>

</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
