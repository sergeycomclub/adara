(function ($) {

    // Helper Functions
    function isInView(element, fullyInView) {
        var pageTop = $(window).scrollTop();
        var pageBottom = pageTop + $(window).height();
        var elementTop = $(element).offset().top;
        var elementBottom = elementTop + $(element).height();

        if (fullyInView === true) {
            return ((elementTop <= pageTop) && (elementBottom >= pageTop));
        } else {
            return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
        }
    }

    function bottomOfThePage() {
        if ((window.innerHeight + window.scrollY - $('.page-footer').height() ) >= document.body.offsetHeight) {
            return true;
        }
        else {
            return false;
        }
    }

    function richedTheFooter() {
        if ((window.innerHeight + window.scrollY ) >= document.body.offsetHeight) {
            return true;
        }
        else {
            return false;
        }
    }

    // Footer section
    function updateScrollBlockFooterMargin() {
        var marginBottom = $('footer.page-footer').outerHeight();
        $(".scroll-block").css({'margin-bottom': marginBottom});
    }


    // Menu Section
    function menuAnimate() {
        $(".primary-menu.animated-menu > li").each(function (index) {

            $(this).addClass('flipInX animated').delay(600 * (index + 1)).queue(function (next) {
                $(this).removeClass('flipInX animated')
                next();
            });
        })
    }

    // Detect on Scroll
    function onScrollUpdate() {

        // Make top menu smaller when article is on screen
        // Down arrow is hidden when article on screen
        if ($(".front-page-content").length > 0) {
            if (isInView($('article'), false)) {
                $('nav.main-top-navigation').addClass('small-top-menu');
                $(".front-page-action-button-down").addClass('hide');
            } else {
                $('nav.main-top-navigation').removeClass('small-top-menu');
                $(".front-page-action-button-down").removeClass('hide');
            }
        } else {
            if ($(window).scrollTop() > 5) {
                $('nav.main-top-navigation').addClass('small-top-menu');
                $(".front-page-action-button-down").addClass('hide');
            } else {
                $('nav.main-top-navigation').removeClass('small-top-menu');
                $(".front-page-action-button-down").removeClass('hide');
            }
        }

        // Hide constellation wrapper when article fully cover screen
        if (isInView($('article'), true)) {
            $('.front-page-intro').css({'opacity': 0, 'visibility': 'hidden'});
        }
        else {
            $('.front-page-intro').css({'opacity': 1, 'visibility': 'visible'});
        }

        if (richedTheFooter($('article'), true)) {
            $('.constellation-grey-wrapper').css({'opacity': 0, 'visibility': 'hidden'});
        }
        else {
            $('.constellation-grey-wrapper').css({'opacity': 1, 'visibility': 'visible'});
        }

        // Step by step show text
        var step = $('article').offset().top / 5,
            distance = step / 4;


        var scrollPosition = $(window).scrollTop();

        if (scrollPosition < step && scrollPosition > (distance / 2)) {
            $('.step-2').addClass('visible');
        } else {
            $('.step-2').removeClass('visible');
        }

        if (scrollPosition < (step * 2 - distance ) && scrollPosition > (step + distance )) {
            $('.step-3').addClass('visible');
        } else {
            $('.step-3').removeClass('visible');
        }

        if (scrollPosition < (step * 3 - distance ) && scrollPosition > (step * 2 + distance )) {
            $('.step-4').addClass('visible');
        } else {
            $('.step-4').removeClass('visible');
        }

        if (scrollPosition < (step * 4 - distance  ) && scrollPosition > (step * 3 + distance )) {
            $('.step-5').addClass('visible');
        } else {
            $('.step-5').removeClass('visible');
        }

        // Footer on mission page
        if ($('.page-template-mission').length > 0)

            if (bottomOfThePage()) {
                $('footer.page-footer').addClass('show-footer');
            } else {
                $('footer.page-footer').removeClass('show-footer').addClass('hide-footer');
            }

    } // On scroll

    function topMenuScroll() {
        $(window).scroll(function (e) {
            onScrollUpdate();
        });
    }


    $(".front-page-action-button-down").click(function (e) {

        $('html, body').animate({
            scrollTop: $('article').offset().top
        }, 500);
    });

    $(".action-button-up").click(function (e) {
        var delay = 1000;

        $('.steps-wrapper').hide();
        $('html, body').animate({
            scrollTop: 0
        }, delay);
        setTimeout(function () {
            $('.steps-wrapper').show();
        }, delay);
    });

    function adjustHeight(parentElement) {
        var elements = $(parentElement).children();
        var maxHeight = 0;

        $.each(elements, function () {
            var $this = $(this);
            if ($this.outerHeight() > maxHeight) {
                maxHeight = $this.outerHeight();
            }
        });

        $(elements).outerHeight(maxHeight);
    }

    function updateAllFeatures() {
        adjustHeight($('.adjustable-elements-3cols'));
        adjustHeight($('.adjustable-elements-4cols'));
        // adjustHeight($('.flex-row-center'));
        menuAnimate();
        updateScrollBlockFooterMargin();
        topMenuScroll();
        onScrollUpdate();
    }

    // $(window).resize(function () {
    //     setTimeout(function(){
    //         updateAllFeatures();
    //
    //     }, 200);
    // });

    $( window ).on( "orientationchange", function( event ) {
            // setTimeout(function(){
                updateAllFeatures();
            // }, 200);
    });

    $(window).load(function () {
        updateAllFeatures();
    });
})(jQuery);
