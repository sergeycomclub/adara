<?php
get_header(); ?>
    <script
        src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
        crossorigin="anonymous"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/constellation.min.js"></script>
    <div id="primary" class="content-area">
        <div class="front-page-intro">
            <div class="constellation-wrapper">
                <h2 class="constellation-title">ADARA RESEARCH
<!--                    <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/adara_research2.svg" alt="">-->
                </h2>
                <img class="constellation-title-img" src="<?php echo get_template_directory_uri(); ?>/img/adara_research2.svg" alt="">
                <h3 class="intro-content step-1">Combining creativity with<br>
                    discipline and statistical rigor</h3>
                <canvas class="constellation"></canvas>
            </div>
            <?
            $slogan_page = get_page_by_path('slogan');
            $slogan_page_id = $slogan_page->ID;
            $content_post = get_post($slogan_page_id);
            $slogan_content = $content_post->post_content;
            $slogan_content = apply_filters('the_content', $slogan_content);
            $slogan_content = str_replace(']]>', ']]&gt;', $slogan_content);
            ?>
            <h4 class="intro-orange-text"><div class="container"><? echo $slogan_content; ?></div></h4>
<!--            <div class="intro-text-wrapper">-->
<!--                <h3 class="intro-content step-1">Combining creativity with<br>-->
<!--                    discipline and statistical rigor</h3>-->
<!--                <div class="steps-wrapper">-->
<!--                    <h4 class="intro-content step-2 steps hidden">We help clients understand, build and develop-->
<!--                        ideas.</h4>-->
<!--                    <h4 class="intro-content step-3 steps hidden">We design a unique framework to analyze a client’s-->
<!--                        idea or product,</h4>-->
<!--                    <h4 class="intro-content step-4 steps hidden">to assess how it works and to propose improvements or-->
<!--                        optimization.</h4>-->
<!--                    <h4 class="intro-content step-5 steps hidden">Their ideas and products are working and how they can-->
<!--                        be optimized further.</h4>-->
<!--                </div>-->
<!--            </div>-->
        </div>
        <script src="<?php echo get_template_directory_uri(); ?>/js/stars-init.js"></script>
        <main id="main" class="site-main front-page-top-indent" role="main">
            <i class="circle material-icons action-down front-page-action-button-down">arrow_downward</i>
            <?php
            // Start the loop.
            while (have_posts()) : the_post();

                // Include the page content template.
                get_template_part('template-parts/content', 'front-page');

            endwhile;
            ?>


        </main><!-- .site-main -->

    </div><!-- .content-area -->
<?php get_footer(); ?>