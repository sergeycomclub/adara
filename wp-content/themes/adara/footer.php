</div>
</div>
<footer class="page-footer">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <?php if (is_active_sidebar('sidebar-1')) : ?>
                    <aside id="secondary" class="sidebar widget-area" role="complementary">
                        <?php dynamic_sidebar('sidebar-1'); ?>
                    </aside><!-- .sidebar .widget-area -->
                <?php endif; ?>


            </div>
            <div class="col l2 m4 s6 line-above">
                <?php if (is_active_sidebar('sidebar-2')) : ?>
                    <div class="widget-area">
                        <?php dynamic_sidebar('sidebar-2'); ?>
                    </div><!-- .widget-area -->
                <?php endif; ?>
            </div>
            <div class="col l2 m4 s6 line-above">
                <?php if (is_active_sidebar('sidebar-21')) : ?>
                    <div class="widget-area">
                        <?php dynamic_sidebar('sidebar-21'); ?>
                    </div><!-- .widget-area -->
                <?php endif; ?>
            </div>
            <div class="col l2 m4 s12">
                <?php if (is_active_sidebar('sidebar-3')) : ?>
                    <div class="widget-area">
                        <?php dynamic_sidebar('sidebar-3'); ?>
                    </div><!-- .widget-area -->
                <?php endif; ?>
            </div>
        </div>
    </div>
</footer>


<!--  Scripts-->
<script src="<?php echo get_template_directory_uri(); ?>/js/materialize.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/init.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>

<?php //wp_footer(); ?>

</body>
</html>
